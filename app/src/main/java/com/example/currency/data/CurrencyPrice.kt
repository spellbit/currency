package com.example.currency.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by stanislav on 2/9/2018.
 */
data class CurrencyPrice(@Expose
                         @SerializedName("currency")
                         var currency: String? = null,

                         @Expose
                         @SerializedName("amount")
                         var amount: Double? = null)