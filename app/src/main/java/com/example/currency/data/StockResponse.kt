package com.example.currency.data

import com.example.currency.data.Currency
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by stanislav on 2/9/2018.
 */
data class StockResponse(@Expose
                         @SerializedName("stock")
                         var stock: List<Currency>? = null)


