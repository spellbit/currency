package com.example.currency.data

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by stanislav on 2/9/2018.
 */

data class Currency(@Expose
                    @SerializedName("name")
                    var name: String? = null,

                    @Expose
                    @SerializedName("price")
                    var price: CurrencyPrice? = null,

                    @Expose
                    @SerializedName("percentChange")
                    var percentChange: Double? = null,

                    @Expose
                    @SerializedName("volume")
                    var volume: Long? = null,

                    @Expose
                    @SerializedName("symbol")
                    var symbol: String? = null)