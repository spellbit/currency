package com.example.currency

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.currency.data.Currency
import kotlinx.android.synthetic.main.item_currency.view.*


/**
 * Created by stanislav on 2/9/2018.
 */
class CurrencyAdapter(var items : List<Currency>) : RecyclerView.Adapter<CurrencyAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_currency, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    override fun getItemCount() = items.size

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        fun bind(currency: Currency){
            with(itemView) {
                tv_name.text = currency.name ?: ""
                tv_volume.text = currency.volume?.toString() ?: ""
                tv_amount.text =  "%.2f".format(currency.price?.amount ?: 0)
            }
        }
    }
}