package com.example.currency

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.example.currency.data.Currency
import com.example.currency.data.StockResponse
import com.github.ajalt.timberkt.Timber
import com.github.ajalt.timberkt.e
import com.rx2androidnetworking.Rx2AndroidNetworking
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber.DebugTree
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {

    private val UPDATE_INTERVAL = 15L
    private val intervalObservable = Observable.interval(UPDATE_INTERVAL, TimeUnit.SECONDS)
    private lateinit var subscription: Disposable

    private val items = mutableListOf<Currency>()
    private val adapter = CurrencyAdapter(items)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Timber.plant(DebugTree())

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        recyclerView.adapter = adapter

        getStock()
        subscription = intervalObservable.subscribe {
            getStock()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            when (it.itemId) {
                R.id.refresh -> {
                    refresh()
                    return true
                }
                else -> { }
            }
        }
        return false
    }

    private fun onStockLoaded(currencies: List<Currency>) {
        if(items.size > 0) {
            Toast.makeText(this, getString(R.string.data_refreshed), Toast.LENGTH_SHORT).show()
        }

        items.clear()
        items.addAll(currencies)
        adapter.notifyDataSetChanged()

    }

    private fun refresh() {
        subscription.dispose()
        getStock()
        subscription = intervalObservable.subscribe {
            getStock()
        }
    }

    private fun getStock() {
        getStockObservable()
                .subscribeOn(Schedulers.single())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ stockResponse ->
                    stockResponse.stock?.let { stock ->
                        onStockLoaded(stock)
                    }
                }, { err ->
                    e(err)
                })
    }

    private fun getStockObservable(): Observable<StockResponse> =
            Rx2AndroidNetworking.get("http://phisix-api3.appspot.com/stocks.json")
                    .build()
                    .getObjectObservable(StockResponse::class.java)
}
