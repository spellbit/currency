package com.example.currency

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import android.content.Intent



class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Flowable.fromCallable({
            Thread.sleep(2000) //  imitate doing stuff
        }).subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(Schedulers.single())
                .subscribe({
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                })

    }
}